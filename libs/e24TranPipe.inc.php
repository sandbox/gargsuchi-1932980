<?php
/*
 * e24TransPipe
 * A modified HDFC specific version of e24PaymentPipe - sgarg at axelerant dot com
 *
 * http://e24payment-php.sourceforge.net/
 *
 * e24payment-php is an implementation in PHP of E24PAymentPipe
 * of italian bank Intesa San Paolo java classes. It allows to
 * connect to online credit card payment from http://www.aciworldwide.com/.
 *
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details at
 * http://www.gnu.org/copyleft/lgpl.html
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 */
// You can download pclzip.lib.php from http://www.phpconcept.net/pclzip/index.en.php
include("pclzip.lib.php");
class e24TransPipe
{
  var $SUCCESS = 0;
  var $FAILURE = -1;
  var $BUFFER = 2320;
  var $strIDOpen = "<id>";
  var $strPasswordOpen = "<password>";
  var $strWebAddressOpen = "<webaddress>";
  var $strPortOpen = "<port>";
  var $strContextOpen = "<context>";
  var $strIDClose = "</id>";
  var $strPasswordClose = "</password>";
  var $strWebAddressClose = "</webaddress>";
  var $strPortClose = "</port>";
  var $strContextClose = "</context>";
  var $webAddress;
  var $port;
  var $id;
  var $password;
  var $action;
  var $transId;
  var $amt;
  var $responseURL;
  var $trackId;
  var $udf1;
  var $udf2;
  var $udf3;
  var $udf4;
  var $udf5;
  var $paymentPage;
  var $paymentId;
  var $result;
  var $auth;
  var $ref;
  var $avr;
  var $date;
  var $currency;
  var $errorURL;
  var $language;
  var $context;
  var $resourcePath;
  var $alias;
  var $error;
  var $rawResponse;
  var $debugMsg;
  function e24TransPipe()
  {
    $this->debugMsg = '';
    $this->id = '';
    $this->type = '';
    $this->card = '';
    $this->expDay = '';
    $this->expMonth = '';
    $this->expYear = '';
    $this->action = '';
    $this->ref = '';
    $this->cardbalance = '';
    $this->cvv2 = '';
    $this->transId = '';
    $this->zip = '';
    $this->addr = '';
    $this->member = '';
    $this->amt = '';
    $this->trackId = '';
    $this->udf1 = '';
    $this->udf2 = '';
    $this->udf3 = '';
    $this->udf4 = '';
    $this->udf5 = '';
    $this->result = '';
    $this->auth = '';
    $this->avr = '';
    $this->date = '';
    $this->webAddress = '';
    $this->port = '';
    $this->context = '';
    $this->currCode = '';
    $this->resourcePath = '';
    $this->alias = '';
    $this->error = '';
  }
  function setId($s)
  {
    $this->id = $s;
  }
  function getId()
  {
    return $this->id;
  }
  function setPassword($s)
  {
    $this->password = $s;
  }
  function getPassword()
  {
    return $this->password;
  }
  function getPayId()
  {
    return $this->payid;
  }
  function setCard($s)
  {
    $this->card = $s;
  }
  function getCard()
  {
    return $this->card;
  }
  function setAction($s)
  {
    $this->action = $s;
  }
  function getAction()
  {
    return $this->action;
  }
  function setRef($s)
  {
    $this->ref = $s;
  }
  function getRef()
  {
    return $this->ref;
  }
  function setCardBalance($s)
  {
    $this->cardbalance = $s;
  }
  function getCardBalance()
  {
    return $this->cardbalance;
  }
  function setTransId($s)
  {
    $this->transId = $s;
  }
  function getTransId()
  {
    return $this->transId;
  }
  function setZip($s)
  {
    $this->zip = $s;
  }
  function getZip()
  {
    return $this->zip;
  }
  function setAddress($s)
  {
    $this->addr = $s;
  }
  function getAddress()
  {
    return $this->addr;
  }
  function setMember($s)
  {
    $this->member = $s;
  }
  function getMember()
  {
    return $this->member;
  }
  function setAmt($s)
  {
    $this->amt = $s;
  }
  function getAmt()
  {
    return $this->amt;
  }
  function setTrackId($s)
  {
    $this->trackId = $s;
  }
  function getTrackId()
  {
    return $this->trackId;
  }
  function setUdf1($s)
  {
    $this->udf1 = $s;
  }
  function getUdf1()
  {
    return $this->udf1;
  }
  function setUdf2($s)
  {
    $this->udf2 = $s;
  }
  function getUdf2()
  {
    return $this->udf2;
  }
  function setUdf3($s)
  {
    $this->udf3 = $s;
  }
  function getUdf3()
  {
    return $this->udf3;
  }
  function setUdf4($s)
  {
    $this->udf4 = $s;
  }
  function getUdf4()
  {
    return $this->udf4;
  }
  function setUdf5($s)
  {
    $this->udf5 = $s;
  }
  function getUdf5()
  {
    return $this->udf5;
  }
  function getResult()
  {
    return $this->result;
  }
  function getAuth()
  {
    return $this->auth;
  }
  function getAvr()
  {
    return $this->avr;
  }
  function getDate()
  {
    return $this->date;
  }
  function setCvv2($s)
  {
    $this->cvv2 = $s;
  }
  function getCvv2()
  {
    return $this->cvv2;
  }
  function setType($s)
  {
    $this->type = $s;
  }
  function getType()
  {
    return $this->type;
  }
  function getWebAddress()
  {
    return $this->webAddress;
  }
  function setWebAddress($s)
  {
    $this->webAddress = $s;
  }
  function getPort()
  {
    return $this->port;
  }
  function setPort($s)
  {
    $this->port = $s;
  }
  function setContext($s)
  {
    $this->context = $s;
  }
  function getErrorMsg()
  {
    return $this->error;
  }
  function getDebugMsg()
  {
    return $this->debugMsg;
  }
  function getExpDay()
  {
    return $this->expDay;
  }
  function setExpDay($s)
  {
    $this->expDay = $s;
  }
  function getExpMonth()
  {
    return $this->expMonth;
  }
  function setExpMonth($s)
  {
    $this->expMonth = $s;
  }
  function getExpYear()
  {
    return $this->expYear;
  }
  function setExpYear($s)
  {
    $this->expYear = $s;
  }
  function getCurrencyCode()
  {
    return $this->currCode;
  }
  function setCurrencyCode($s)
  {
    $this->currCode = $s;
  }
  function getResourcePath()
  {
    return $this->resourcePath;
  }
  function setResourcePath($s)
  {
    $this->resourcePath = $s;
  }
  function getAlias()
  {
    return $this->alias;
  }
  function setAlias($s)
  {
    $this->alias = $s;
  }
  function performTransaction()
  {
    $stringbuffer = "";
    if (!$this->getSecureSettings())
      return -1;
    $stringbuffer = $this->buildRequestString();
    $s = $this->sendMessage($stringbuffer, "TranPortalXMLServlet");
    if ($s == null)
      return -1;
    $arraylist = $this->parseResults($s);
    if ($arraylist == null) {
      return -1;
    }
		else {
      foreach ($arraylist AS $key => $val)
        $this->$key = $val;
      return 0;
    }
  }
  function sendMessage($s, $s1)
  {
    $stringbuffer = "";
    $this->debugMsg .= ("\n---------- " . $s1 . ": " . time() . " ----------");
    if ($this->port == "443") {
      if (strlen($this->webAddress) <= 0) {
        $error = "No URL specified.";
        return null;
      }
      if ($this->port == "443")
        $stringbuffer .= ("https://");
      else
        $stringbuffer .= ("http://");
      $stringbuffer .= ($this->webAddress);
      if (strlen($this->port) > 0) {
        $stringbuffer .= (":");
        $stringbuffer .= ($this->port);
      }
      if (strlen($this->context) > 0) {
        if (!$this->StartsWith($this->context, "/"))
          $stringbuffer .= ("/");
        $stringbuffer .= ($this->context);
        if (!$this->EndsWith($this->context, "/"))
          $stringbuffer .= ("/");
      }
			else {
        $stringbuffer .= ("/");
      }
      $stringbuffer .= ("servlet/");
      $stringbuffer .= ($s1);
      $this->debugMsg .= ("\nAbout to create the URL to: " . $stringbuffer);
      $url = $stringbuffer;
      $this->debugMsg .= ("\nAbout to create http connection");
      $this->debugMsg .= ("\nCreated connection");
      if (strlen($s) > 0) {
        $c = curl_init();
        curl_setopt($c, CURLOPT_URL, $stringbuffer);
        curl_setopt($c, CURLOPT_POST, true);
        curl_setopt($c, CURLOPT_POSTFIELDS, $s);
        $this->debugMsg .= ("\nabout to write DataOutputSteam");
        $this->debugMsg .= ("\nafter DataOutputStream");
        curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
        $tmp = curl_exec($c);
        curl_close($c);
        $this->rawResponse = $tmp;
        $this->debugMsg .= ("\nReceived RESPONSE: " . $this->rawResponse);
        return $this->rawResponse;
      }
			else {
        $error = "No Data To Post!";
      }
    } {
      $this->clearFields();
      $this->error = "Failed to make connection:\n" . $exception;
      return null;
    }
  }
  function parseSimpleXML($s)
  {
    if (substr($s, 0, 1) != "<")
      return; // this is not XML
    $s = trim($s);
    $i = 0;
    $j = strlen($s);
    do {
      $k = strpos($s, ">", $i + 1);
      if ($k == -1)
        break;
      $s1 = substr($s, $i + 1, $k - $i - 1);
      if ($s1 == false)
        break;
      $s2 = "</" . $s1 . ">";
      $l = strpos($s, $s2);
      $s3 = substr($s, $k + 1, $l - $k - 1);
      //      if ($s3 == false) break;
      $hash_map[$s1] = $s3;
      if ($l + strlen($s2) + 1 < strlen($s))
        $i = strpos($s, "<", $l + strlen($s2) + 1);
      else
        $i = -1;
    } while ($i >= 0);
    return $hash_map;
  }
  function clearResults()
  {
    $this->result = "";
    $this->auth = "";
    $this->ref = "";
    $this->cardbalance = "";
    $this->avr = "";
    $this->date = "";
    $this->transId = "";
    $this->trackId = "";
    $this->udf1 = "";
    $this->udf2 = "";
    $this->udf3 = "";
    $this->udf4 = "";
    $this->udf5 = "";
    $this->error = "";
  }
  function clearAllFields()
  {
    $this->id = "";
    $this->payid = "";
    $this->password = "";
    $this->type = "";
    $this->card = "";
    $this->expDay = "";
    $this->expMonth = "";
    $this->expYear = "";
    $this->action = "";
    $this->ref = "";
    $this->cardbalance = "";
    $this->cvv2 = "";
    $this->transId = "";
    $this->zip = "";
    $this->addr = "";
    $this->member = "";
    $this->amt = "";
    $this->trackId = "";
    $this->udf1 = "";
    $this->udf2 = "";
    $this->udf3 = "";
    $this->udf4 = "";
    $this->udf5 = "";
    $this->result = "";
    $this->auth = "";
    $this->avr = "";
    $this->date = "";
    $this->webAddress = "";
    $this->port = "";
    $this->context = "";
    $this->currCode = "";
    $this->alias = "";
    $this->resourcePath = "";
    $this->error = "";
  }
  function buildRequestString()
  {
    $out = '';
    if ($this->id)
      $out .= "<id>" . $this->id . "</id>";
    if ($this->password)
      $out .= "<password>" . $this->password . "</password>";
    if ($this->card)
      $out .= "<card>" . $this->card . "</card>";
    if ($this->expDay)
      $out .= "<expday>" . $this->expDay . "</expday>";
    if ($this->expMonth)
      $out .= "<expmonth>" . $this->expMonth . "</expmonth>";
    if ($this->expYear)
      $out .= "<expyear>" . $this->expYear . "</expyear>";
    if ($this->currCode)
      $out .= "<currencycode>" . $this->currCode . "</currencycode>";
    if ($this->action)
      $out .= "<action>" . $this->action . "</action>";
    if ($this->ref)
      $out .= "<ref>" . $this->ref . "</ref>";
    if ($this->transId)
      $out .= "<transid>" . $this->transId . "</transid>";
    if ($this->password)
      $out .= "<zip>" . $this->zip . "</zip>";
    if ($this->addr)
      $out .= "<addr>" . $this->addr . "</addr>";
    if ($this->member)
      $out .= "<member>" . $this->member . "</member>";
    if ($this->amt)
      $out .= "<amt>" . $this->amt . "</amt>";
    if ($this->trackId)
      $out .= "<trackid>" . $this->trackId . "</trackid>";
    if ($this->udf1)
      $out .= "<udf1>" . $this->udf1 . "</udf1>";
    if ($this->udf2)
      $out .= "<udf2>" . $this->udf2 . "</udf2>";
    if ($this->udf3)
      $out .= "<udf3>" . $this->udf3 . "</udf3>";
    if ($this->udf4)
      $out .= "<udf4>" . $this->udf4 . "</udf4>";
    if ($this->udf5)
      $out .= "<udf5>" . $this->udf5 . "</udf5>";
    if ($this->type)
      $out .= "<type>" . $this->type . "</type>";
    if ($this->cvv2)
      $out .= "<cvv2>" . $this->cvv2 . "</cvv2>";
    return $out;
  }
  function parseResults($s)
  {
    $arraylist; {
      if ($this->StartsWith($s, "!ERROR!")) {
        $error = $s;
        return null;
      }
      $tokens = $this->parseSimpleXML($s);
      return ($tokens);
    }
    $this->error = "Internal Error!";
    return null;
  }
  function getSecureSettings()
  {
    $s = "";
    if (!$this->createReadableZip())
      return false;
    $s = $this->readZip();
    if ($s == "")
      return false;
    unlink($this->getResourcePath() . "resource.cgz");
    return $this->parseSettings($s);
  }
  function parseSettings($s)
  {
    $i = 0;
    $j = 0;
    $i = strpos($s, "<id>") + strlen("<id>");
    $j = strpos($s, "</id>");
    $this->setId(substr($s, $i, $j - $i));
    $i = strpos($s, "<password>") + strlen("<password>");
    $j = strpos($s, "</password>");
    $this->setPassword(substr($s, $i, $j - $i));
    $i = strpos($s, "<webaddress>") + strlen("<webaddress>");
    $j = strpos($s, "</webaddress>");
    $this->setWebAddress(substr($s, $i, $j - $i));
    $i = strpos($s, "<port>") + strlen("<port>");
    $j = strpos($s, "</port>");
    $this->setPort(substr($s, $i, $j - $i));
    $i = strpos($s, "<context>") + strlen("<context>");
    $j = strpos($s, "</context>");
    $this->setContext(substr($s, $i, $j - $i));
    return true;
  }
  function simpleXOR($abyte0)
  {
    $s = "Those who profess to favour freedom and yet depreciate agitation are men who want rain without thunder and lightning";
    $abyte1 = $this->getBytes($s);
    for ($i = 0; $i < sizeof($abyte0); ) {
      for ($j = 0; $j < sizeof($abyte1); $j++) {
        $abyte2[$i] = ($abyte0[$i] ^ $abyte1[$j]);
        if (++$i == sizeof($abyte0))
          break;
      }
    }
    return $abyte2;
  }
  function getBytes($s)
  {
    $hex_ary = array();
    $size = strlen($s);
    for ($i = 0; $i < $size; $i++)
      $hex_ary[] = chr(ord($s[$i]));
    return $hex_ary;
  }
  function getString($byteArray)
  {
    $s = "";
    foreach ($byteArray as $byte) {
      $s .= $byte;
    }
    return $s;
  }
  function StartsWith($Haystack, $Needle)
  {
    // Recommended version, using strpos
    return strpos($Haystack, $Needle) === 0;
  }
  function EndsWith($Haystack, $Needle)
  {
    // Recommended version, using strpos
    return strrpos($Haystack, $Needle) === strlen($Haystack) - strlen($Needle);
  }
  function createReadableZip()
  {
    $filenameInput = $this->getResourcePath() . "resource.cgn";
    $handleInput = fopen($filenameInput, "r");
    $contentsInput = fread($handleInput, filesize($filenameInput));
    $filenameOutput = $this->getResourcePath() . "resource.cgz";
    //            @unlink($filenameOutput);
    $handleOutput = fopen($filenameOutput, "w");
    $inByteArray = $this->getBytes($contentsInput);
    $outByteArray = $this->simpleXOR($inByteArray);
    fwrite($handleOutput, $this->getString($outByteArray));
    fclose($handleInput);
    fclose($handleOutput);
    return true;
  }
  function readZip()
  {
    $s = "";
    $filenameInput = $this->getResourcePath() . "resource.cgz";
    $zip = new PclZip($filenameInput);
    $zipentry;
    $names = $zip->listContent();
    $i = 0;
    foreach ($names as $name) {
      if ($name["filename"] == $this->getAlias() . ".xml") {
        $zip->extractByIndex($i, $this->getResourcePath());
        //$xmlNameInput = $name["filename"];
        $xmlNameInput = $this->getResourcePath() . $name["filename"];
        $xmlHandleInput = fopen($xmlNameInput, "r");
        $xmlContentsInput = fread($xmlHandleInput, filesize($xmlNameInput));
        fclose($xmlHandleInput);
        unlink($xmlNameInput);
        $s = $xmlContentsInput;
        $s = $this->getString($this->simpleXOR($this->getBytes($s)));
        break;
      }
      $i++;
    }
    return $s;
  }
}