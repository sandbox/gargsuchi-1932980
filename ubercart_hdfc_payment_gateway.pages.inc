<?php
// $Id$

/**
 * @file
 * HDFC administration menu items.
 */

// Process Postbacks from ubercart_hdfc_payment_gateway.
function ubercart_hdfc_payment_gateway_redirect($order_id = 0)
{
  global $conf;
  global $base_url;
  $ErrorTx = isset($_POST['Error']) ? $_POST['Error'] : '';
  $ErrorResult = isset($_POST['ErrorText']) ? $_POST['ErrorText'] : '';
  $payID = isset($_POST['paymentid']) ? $_POST['paymentid'] : '';
  
  if ($ErrorTx == '') {
    $result = isset($_POST['result']) ? $_POST['result'] : '';
    $postdate = isset($_POST['postdate']) ? $_POST['postdate'] : '';
    $details = isset($_POST['udf1']) ? $_POST['udf1'] : '';
    $tranid = isset($_POST['tranid']) ? $_POST['tranid'] : '';
    $auth = isset($_POST['auth']) ? $_POST['auth'] : '';
    $trackid = isset($_POST['trackid']) ? $_POST['trackid'] : '';
    
    if ($payID != '') {
      db_query("INSERT INTO {ubercart_hdfc_payment_gateway_pg_response} (paymentid, result, error_result, postdate, tranid, auth, trackid, error_txt) VALUES ('%s', '%s', '', '%s', '%s', '%s', %d, '')", $payID, $result, $postdate, $tranid, $auth, $trackid);
    }
    
    //header('Location:'.$base_url.'/ubercart_hdfc_payment_gateway/success?paymentid='.$payID); //This fails for HDFC
    //drupal_goto('ubercart_hdfc_payment_gateway/success', 'paymentid='.$payID, NULL, 307); //This fails for HDFC
		
		//Below is the directive used by HDFC to know Redirection URL. It is not a PHP/HTML/JavaScript standarad predefined way of redirection. This is specifically HDFC defined.
    $REDIRECT = 'REDIRECT=' . $base_url . '/ubercart_hdfc_payment_gateway/success?paymentid=' . $payID;
    echo $REDIRECT;
  }
	else {
    $result = isset($_POST['result']) ? $_POST['result'] : '';
    
    if ($payID != '') {
      db_query("INSERT INTO {ubercart_hdfc_payment_gateway_pg_response} (paymentid, result, error_result, postdate, tranid, auth, trackid, error_txt) VALUES ('%s', '%s', '%s', '%s', '%s', '%s', %d, '%s')", $payID, $result, $ErrorResult, $postdate, $tranid, $auth, $trackid, $ErrorTx);
    }
    
    //    header('Location:'.$base_url.'/ubercart_hdfc_payment_gateway/success?paymentid='.$payID); //This fails for HDFC
		
    //    drupal_goto('ubercart_hdfc_payment_gateway/success', 'paymentid='.$payID, NULL, 307); //This fails for HDFC
		
		//Below is the directive used by HDFC to know Redirection URL. It is not a PHP/HTML/JavaScript standarad predefined way of redirection. This is specifically HDFC defined.
    $REDIRECT = 'REDIRECT=' . $base_url . '/ubercart_hdfc_payment_gateway/success?paymentid=' . $payID;
    echo $REDIRECT;
  }
  
}

// Handles a complete HDFC Payment sale.
function ubercart_hdfc_payment_gateway_success()
{
  /* // Example response
  [paymentid] => 4797341431400550
  [result] => CAPTURED
  [postdate] => 0225
  [tranid] => 859719441400550
  [auth] => 999999
  [trackid] => 1141
  */
  
  if ($_GET['paymentid'] != '') {
    $pgResponse = db_query("SELECT paymentid, result, error_result, postdate, tranid, auth, trackid, error_txt FROM {ubercart_hdfc_payment_gateway_pg_response} WHERE paymentid = '%s'", $_GET['paymentid']);
    
    $pgResponseParams = db_fetch_array($pgResponse);
    
    $status = $pgResponseParams['result'];
    foreach ($pgResponseParams AS $key => $val)
      $$key = $val;
    
    if (($status == 'APPROVED') || ($status == 'CAPTURED')) {
      //all went well - success
      $order_id = $pgResponseParams['trackid'];
      if (intval($_SESSION['cart_order']) != $order_id) {
        $_SESSION['cart_order'] = $order_id;
      }
      
      if (!($order = uc_order_load($order_id))) {
        drupal_goto('cart');
      }
      
      $amount = $order->order_total;
      
      // all is well - now save stuff to DB
      // save it in ubercart_hdfc_payment_gateway
      
      db_query("INSERT INTO {ubercart_hdfc_payment_gateway} (order_id, amount, payment_id, status, postdate, transaction_id, auth_id, track_id) VALUES (%d, '%s', '%s', '%s', now(), '%s', '%s', '%s')", $order_id, $amount, $paymentid, $status, $tranid, $auth, $trackid);
      
      
      $comment = t('HDFC payment for ' . $order_id . ' worked.');
      uc_payment_enter($order_id, 'hdfc', $amount, $order->uid, NULL, $comment);
      
      $context['subject'] = array(
        'cart_item' => $item,
        'node' => node_load($item->nid),
      );
      
      uc_order_comment_save($order_id, 0, t('Payment of @amount @currency submitted through HDFC.', array('@amount' => $amount, '@currency' => 'INR')));
      //uc_cart_complete_sale($order);
      
      // This lets us know it's a legitimate access of the complete page.
      $_SESSION['do_complete'] = TRUE;
      $_SESSION['cart_order'] = $order_id; // again
      
      //db_query("DELETE FROM {ubercart_hdfc_payment_gateway_pg_response} where paymentid='%s'", $paymentid);
      
      drupal_goto('cart/checkout/complete');
    }
		else {
      // handle failure here
      $order_id = $pgResponseParams['trackid'];
      $status = $status . " - " . $error_result . " - " . $error_txt;
      
      db_query("INSERT INTO {ubercart_hdfc_payment_gateway} (order_id, amount, payment_id, status, postdate, transaction_id, auth_id, track_id) VALUES (%d, '%s', '%s', '%s', now(), '%s', '%s', '%s')", $order_id, $amount, $paymentid, $status, $tranid, $auth, $trackid);
      
      uc_order_update_status($order_id, 'error');
      
      watchdog('ubercart_hdfc_payment_gateway', 'Error returned from HDFC. Cart order ID: @cart_order. Error Txt: @error_txt. Error Result: @error_result', array('@cart_order' => $_SESSION['cart_order'],'@error_txt' => $error_txt, '@error_result' => $error_result), WATCHDOG_ERROR);
      
      //db_query("DELETE FROM {ubercart_hdfc_payment_gateway_pg_response} where paymentid='%s'", $paymentid);
      
      if (strcmp($result, "NOT CAPTURED") == 0) {
        drupal_set_message(t("Invalid information entered at HDFC. Please try again with correct details."), 'error');
      } else if (strcmp($result, "DENIED BY RISK") == 0) {
        drupal_set_message(t("Your card is blocked by HDFC. Please try again with correct detail."), 'error');
      } else if (strcmp($result, "CANCELED") == 0) {
        drupal_set_message(t("Your HDFC payment was cancelled. Please feel free to continue shopping or contact us for assistance."), 'error');
      }
			else {
        drupal_set_message(t("We're sorry.  We are not able to process your payment. Please try later."), 'error');
      }
      
      drupal_goto('cart/checkout');
    }
  }
	else {
    drupal_set_message(t("Invalid Payment information entered at HDFC. Please try again with correct details."), 'error');
    
    drupal_goto('cart/checkout');
  }
}

// Handles a canceled HDFC Payment sale.
function ubercart_hdfc_payment_gateway_cancel()
{
  unset($_SESSION['cart_order']);
  
  drupal_set_message(t('Your HDFC payment was cancelled. Please feel free to continue shopping or contact us for assistance.'));
  
  drupal_goto('cart');
}
