<?php
// $Id$

/**
 * @file
 * HDFC administration menu items.
 *
 */

function ubercart_hdfc_payment_gateway_settings()
{
  $form = array();
  
  $form['hdfc_currencies'] = array(
    '#type' => 'fieldset',
    '#title' => t('Currency settings'),
    '#tree' => TRUE,
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  
  $hdfcCurrencies = db_query("SELECT * from {ubercart_hdfc_payment_gateway_currency}");
  $currencyCnt = 0;
  while ($hdfcCurrency = db_fetch_array($hdfcCurrencies)) {
    $form['hdfc_currencies'][$currencyCnt] = array(
      '#type' => 'fieldset',
      '#title' => t('Settings for currency ') . $hdfcCurrency['currency_code'],
      '#tree' => TRUE,
      '#collapsible' => TRUE,
      '#collapsed' => (($hdfcCurrency['currency_enable'] == 1) ? FALSE : TRUE),
    );
    
    $form['hdfc_currencies'][$currencyCnt]['currency_code'] = array(
      '#type' => 'hidden',
      '#default_value' => $hdfcCurrency['currency_code'],
    );
    
    $form['hdfc_currencies'][$currencyCnt]['currency_alias'] = array(
      '#title' => t('Alias'),
      '#description' => t('HDFC Alias'),
      '#type' => 'textfield',
      '#default_value' => $hdfcCurrency['currency_alias'],
    );
    
    $form['hdfc_currencies'][$currencyCnt]['currency_password'] = array(
      '#type' => 'password',
      '#title' => t('Password'),
      '#description' => t('Your password'),
      '#default_value' => $hdfcCurrency['currency_password'],
    );
    
    $form['hdfc_currencies'][$currencyCnt]['currency_resource_path'] = array(
      '#type' => 'textfield',
      '#title' => t('Resource Path'),
      '#description' => t('The filesystem path of the resource file. Please add / in the end'),
      '#default_value' => $hdfcCurrency['currency_resource_path'],
    );
    
    $form['hdfc_currencies'][$currencyCnt]['currency_base'] = array(
      '#type' => 'checkbox',
      '#title' => t('Base Currency'),
      '#default_value' => $hdfcCurrency['currency_base'],
    );
    
    $form['hdfc_currencies'][$currencyCnt]['currency_base_change'] = array(
      '#type' => 'textfield',
      '#title' => t('Currency rate from base currency'),
      '#description' => t('The value with which base currency to be multipled to get this currency amount.'),
      '#default_value' => $hdfcCurrency['currency_base_change'],
    );
    
    $form['hdfc_currencies'][$currencyCnt]['currency_enable'] = array(
      '#type' => 'checkbox',
      '#title' => t('Enable Currency'),
      '#default_value' => $hdfcCurrency['currency_enable'],
    );
    
    ++$currencyCnt;
  }
  
  $form['ubercart_hdfc_payment_gateway_language'] = array(
    '#type' => 'hidden',
    '#default_value' => 'USA',
  );
  
  $form['ubercart_hdfc_payment_gateway_success_url'] = array(
    '#type' => 'textfield',
    '#title' => t('Success URL'),
    '#description' => t('The Success URL. Realtive to the base drupal install.'),
    '#default_value' => variable_get('ubercart_hdfc_payment_gateway_success_url', ''),
  );
  
  $form['ubercart_hdfc_payment_gateway_error_url'] = array(
    '#type' => 'textfield',
    '#title' => t('Error URL'),
    '#description' => t('The error URL. Realtive to the base drupal install.'),
    '#default_value' => variable_get('ubercart_hdfc_payment_gateway_error_url', ''),
  );
  
  $form['ubercart_hdfc_payment_gateway_checkout_button'] = array(
    '#type' => 'textfield',
    '#title' => t('Order review submit button text'),
    '#description' => t('Provide HDFC specific text for the submit button on the order review page.'),
    '#default_value' => variable_get('ubercart_hdfc_payment_gateway_checkout_button', t('Submit Order')),
  );
  
  
  $form['ubercart_hdfc_payment_gateway_debug'] = array(
    '#type' => 'checkbox',
    '#title' => t('Show debug info in the logs for Transactions'),
    '#default_value' => variable_get('ubercart_hdfc_payment_gateway_debug', FALSE),
  );
  
  $form['#submit'][] = 'ubercart_hdfc_payment_gateway_settings_submit';
  
  return system_settings_form($form);
}

function ubercart_hdfc_payment_gateway_settings_validate($form, &$form_state)
{
  $baseCnt = 0;
  $enabledCnt = 0;
  
  foreach ($form_state['values']['hdfc_currencies'] as $key => $currencySettings) {
    if ($currencySettings['currency_enable']) {
      $trimmedValue = trim($currencySettings['currency_alias']);
      if (empty($trimmedValue)) {
        form_set_error('hdfc_currencies][' . $key . '][currency_alias', t('Alias is required for enabled Currency.'));
      }
      
      $trimmedValue = trim($currencySettings['currency_resource_path']);
      if (empty($trimmedValue)) {
        form_set_error('hdfc_currencies][' . $key . '][currency_resource_path', t('Resource Path is required for enabled Currency.'));
      }
      
      $trimmedValue = trim($currencySettings['currency_base_change']);
      if (empty($trimmedValue)) {
        form_set_error('hdfc_currencies][' . $key . '][currency_base_change', t('Currency exchange rate is required for enabled Currency.'));
      }
      
      if ($currencySettings['currency_base'] == 1) {
        ++$baseCnt;
      }
      
      if ($currencySettings['currency_enable'] == 1) {
        ++$enabledCnt;
      }
    }
  }
  
  if ($baseCnt > 1) {
    form_set_error('', t('Only one currency can be a base currency.'));
  }
  
  if ($enabledCnt == 0) {
    form_set_error('', t('At least one currency must be enalbed.'));
  }
  
  $trimmedValue = trim($form_state['values']['ubercart_hdfc_payment_gateway_success_url']);
  if (empty($trimmedValue)) {
    form_set_error('ubercart_hdfc_payment_gateway_success_url', t('Success URL is required.'));
  }
  
  $trimmedValue = trim($form_state['values']['ubercart_hdfc_payment_gateway_error_url']);
  if (empty($trimmedValue)) {
    form_set_error('ubercart_hdfc_payment_gateway_error_url', t('Error URL is required.'));
  }
}

function ubercart_hdfc_payment_gateway_settings_submit($form, &$form_state)
{
  variable_set('ubercart_hdfc_payment_gateway_language', $form_state['values']['ubercart_hdfc_payment_gateway_language']);
  variable_set('ubercart_hdfc_payment_gateway_success_url', $form_state['values']['ubercart_hdfc_payment_gateway_success_url']);
  variable_set('ubercart_hdfc_payment_gateway_error_url', $form_state['values']['ubercart_hdfc_payment_gateway_error_url']);
  variable_set('ubercart_hdfc_payment_gateway_checkout_button', $form_state['values']['ubercart_hdfc_payment_gateway_checkout_button']);
  variable_set('ubercart_hdfc_payment_gateway_debug', $form_state['values']['ubercart_hdfc_payment_gateway_debug']);
  
  foreach ($form_state['values']['hdfc_currencies'] as $key => $currencySettings) {
    $currencySettings['currency_password'] = trim($currencySettings['currency_password']);
    $passQuery = (empty($currencySettings['currency_password']) ? '' : ", currency_password='" . $currencySettings['currency_password'] . "'");
    
    db_query("UPDATE {ubercart_hdfc_payment_gateway_currency} set currency_alias='" . $currencySettings['currency_alias'] . "', currency_resource_path='" . $currencySettings['currency_resource_path'] . "', currency_base_change='" . $currencySettings['currency_base_change'] . "', currency_base='" . $currencySettings['currency_base'] . "', currency_enable='" . $currencySettings['currency_enable'] . "'" . $passQuery . " where currency_code='" . $currencySettings['currency_code'] . "'");
  }
}
